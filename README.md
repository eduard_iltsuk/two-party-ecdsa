# Proof of concept Two-Party ECDSA implementation

Requires Java 8 installed

1.) Clone project

2.) Go to project directory and execute

On Windows:
```
mvnw.cmd clean package
```
On UNIX:
```
mvnw clean package
```
3.) Execute jar to see the sample Two-Party ECDSA key exchange and signing:
```
java -jar target\two-party-ecdsa-1.0.0.jar
```
