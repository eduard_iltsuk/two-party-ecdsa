package ee.ut.msc.ecdsa2p;

import static ee.ut.msc.ecdsa2p.util.MathUtils.randomInt;
import static ee.ut.msc.ecdsa2p.util.MathUtils.sha3_256;
import static java.math.BigInteger.ZERO;

import java.math.BigInteger;

import ee.ut.msc.ecdsa2p.crypto.ec.ECCrypto;
import ee.ut.msc.ecdsa2p.crypto.ec.ECDSAPartialSignature;
import ee.ut.msc.ecdsa2p.crypto.ec.ECDSASignature;
import ee.ut.msc.ecdsa2p.crypto.ec.ECKeyPair;
import ee.ut.msc.ecdsa2p.crypto.ec.EllipticCurve;
import ee.ut.msc.ecdsa2p.crypto.paillier.PaillierCrypto;
import ee.ut.msc.ecdsa2p.crypto.paillier.PaillierKeyPair;
import ee.ut.msc.ecdsa2p.crypto.paillier.PaillierPublicKey;
import org.bouncycastle.jce.interfaces.ECPrivateKey;
import org.bouncycastle.jce.interfaces.ECPublicKey;
import org.bouncycastle.math.ec.ECPoint;

public class Server {

    private ECPrivateKey    privateKey;
    private PaillierKeyPair paillierKeyPair;
    private ECPublicKey     commonPublicKey;

    // Temporarily stored parameters only for the duration of signing session
    private BigInteger kServer;
    private ECPoint    rServer;
    private BigInteger r;

    public ECKeyPair generateECKeyPair() {
        ECKeyPair keyPair = ECCrypto.generateKeyPair();
        this.privateKey = keyPair.getPrivateKey();
        return keyPair;
    }

    public PaillierKeyPair generatePaillierKeyPair() {
        PaillierKeyPair paillierKeyPair = PaillierCrypto.generateKeyPair();
        this.paillierKeyPair = paillierKeyPair;
        return paillierKeyPair;
    }

    public BigInteger getPaillierEncryptedServerPrivateKey() {
        return PaillierCrypto.encrypt(privateKey.getD(), paillierKeyPair.getPublicKey());
    }

    public ECPublicKey calculateCommonPublicKey(ECPublicKey mobilePublicKey) {
        this.commonPublicKey = ECCrypto.publicKeyFrom(mobilePublicKey.getQ().multiply(privateKey.getD()));
        return commonPublicKey;
    }

    public ECPoint generateServerKAndR() {
        kServer = randomInt(ZERO, EllipticCurve.PARAMS.getN());
        rServer = EllipticCurve.PARAMS.getG().multiply(kServer);
        return rServer;
    }

    // s = dec(s') * k^(-1)
    public ECDSASignature generateSignature(ECDSAPartialSignature partialSignature) {
        ECPoint commonRPoint = partialSignature.getPartialR().multiply(kServer).normalize();
        BigInteger rX = commonRPoint.getAffineXCoord().toBigInteger();
        r = rX.mod(EllipticCurve.PARAMS.getN());

        BigInteger s = PaillierCrypto.decrypt(partialSignature.getEncryptedPartialS(), paillierKeyPair)
                                     .multiply(kServer.modInverse(EllipticCurve.PARAMS.getN()))
                                     .mod(EllipticCurve.PARAMS.getN());

        // Ensure minimum 's' is used
        BigInteger minS = s.min(EllipticCurve.PARAMS.getN().subtract(s));

        return new ECDSASignature(r, minS);
    }

}
