package ee.ut.msc.ecdsa2p.crypto.paillier;

import lombok.Data;

@Data
public class PaillierKeyPair {

    private final PaillierPublicKey  publicKey;
    private final PaillierPrivateKey privateKey;

}
