package ee.ut.msc.ecdsa2p.crypto.ec;

import java.math.BigInteger;

import lombok.Data;
import org.bouncycastle.math.ec.ECPoint;

@Data
public class ECDSAPartialSignature {

    public final ECPoint    partialR;
    public final BigInteger encryptedPartialS;

}
