package ee.ut.msc.ecdsa2p.crypto.ec;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.spec.ECGenParameterSpec;

import lombok.SneakyThrows;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.signers.ECDSASigner;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jce.interfaces.ECPrivateKey;
import org.bouncycastle.jce.interfaces.ECPublicKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.math.ec.ECPoint;

public class ECCrypto {

    private static final String ALGORITHM = "ECDSA";

    @SneakyThrows
    public static ECKeyPair generateKeyPair() {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM, BouncyCastleProvider.PROVIDER_NAME);
        ECGenParameterSpec ecGenParameterSpec = new ECGenParameterSpec(EllipticCurve.CURVE_TYPE);
        keyPairGenerator.initialize(ecGenParameterSpec, new SecureRandom());

        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        return new ECKeyPair((ECPrivateKey) keyPair.getPrivate(),
                             (ECPublicKey) keyPair.getPublic());
    }

    public static ECPublicKey publicKeyFrom(ECPoint ecPoint) {
        ECPublicKeyParameters ecPublicKeyParameters = new ECPublicKeyParameters(ecPoint, EllipticCurve.PARAMS);
        ECNamedCurveParameterSpec parameterSpec = new ECNamedCurveParameterSpec(EllipticCurve.CURVE_TYPE, EllipticCurve.PARAMS.getCurve(),
                                                                                EllipticCurve.PARAMS.getG(), EllipticCurve.PARAMS.getN());
        return new BCECPublicKey(ALGORITHM, ecPublicKeyParameters, parameterSpec, BouncyCastleProvider.CONFIGURATION);
    }

    public static BigInteger calculateR(BigInteger k) {
        ECPoint R = EllipticCurve.PARAMS.getG().multiply(k).normalize();
        BigInteger rX = R.getAffineXCoord().toBigInteger();
        return rX.mod(EllipticCurve.PARAMS.getN());
    }

    public static boolean verify(byte[] message, ECDSASignature signature, ECPublicKey publicKey) {
        ECDSASigner signer = new ECDSASigner();
        signer.init(false, new ECPublicKeyParameters(publicKey.getQ(), EllipticCurve.PARAMS));
        try {
            return signer.verifySignature(message, signature.getR(), signature.getS());
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
    }

}
