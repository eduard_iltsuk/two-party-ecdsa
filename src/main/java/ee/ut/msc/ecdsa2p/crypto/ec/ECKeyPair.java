package ee.ut.msc.ecdsa2p.crypto.ec;

import lombok.Data;
import org.bouncycastle.jce.interfaces.ECPrivateKey;
import org.bouncycastle.jce.interfaces.ECPublicKey;

@Data
public class ECKeyPair {

    private final ECPrivateKey privateKey;
    private final ECPublicKey  publicKey;

}
