package ee.ut.msc.ecdsa2p.crypto.rsa;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import lombok.Data;

@Data
public class RSAKeyPair {

    private final RSAPrivateKey privateKey;
    private final RSAPublicKey  publicKey;

}
