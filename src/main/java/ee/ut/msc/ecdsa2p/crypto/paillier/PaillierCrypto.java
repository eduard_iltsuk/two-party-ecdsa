package ee.ut.msc.ecdsa2p.crypto.paillier;

import static ee.ut.msc.ecdsa2p.util.MathUtils.randomCoprimeIntLessThan;
import static ee.ut.msc.ecdsa2p.util.MathUtils.randomPrimeInt;
import static java.math.BigInteger.ONE;

import java.math.BigInteger;
import java.util.stream.Stream;

public class PaillierCrypto {

    public static final int SUGGESTED_KEY_LENGTH = 3072;

    public static PaillierKeyPair generateKeyPair() {
        return generateKeyPair(SUGGESTED_KEY_LENGTH);
    }

    // Implemented with optimization according to
    // https://repo.zenk-security.com/Cryptographie%20.%20Algorithmes%20.%20Steganographie/Introduction%20to%20Modern%20Cryptography.pdf
    public static PaillierKeyPair generateKeyPair(int keyLength) {
        BigInteger p = randomPrimeInt(keyLength / 2);
        BigInteger q;
        do {
            q = randomPrimeInt(keyLength / 2);
        } while (q.compareTo(p) == 0);

        BigInteger phi_n = p.subtract(ONE).multiply(q.subtract(ONE));

        BigInteger n = p.multiply(q);
        BigInteger g = n.add(ONE);
        BigInteger lambda = phi_n;
        BigInteger mu = lambda.modInverse(n);

        PaillierPublicKey publicKey = new PaillierPublicKey(n, g);
        PaillierPrivateKey privateKey = new PaillierPrivateKey(lambda, mu);
        return new PaillierKeyPair(publicKey, privateKey);
    }

    // g^m * r^n mod n^2
    public static BigInteger encrypt(BigInteger message, PaillierPublicKey publicKey) {
        BigInteger n = publicKey.getN();
        BigInteger g = publicKey.getG();
        BigInteger r = randomCoprimeIntLessThan(n);
        BigInteger nSquare = publicKey.getNSquare();

        return g.modPow(message, nSquare)
                .multiply(r.modPow(n, nSquare))
                .mod(nSquare);
    }

    // L(c^lambda mod n^2) * mu mod n
    public static BigInteger decrypt(BigInteger cipherText, PaillierKeyPair keyPair) {
        PaillierPublicKey publicKey = keyPair.getPublicKey();
        PaillierPrivateKey privateKey = keyPair.getPrivateKey();

        BigInteger n = publicKey.getN();
        BigInteger mu = privateKey.getMu();
        BigInteger lambda = privateKey.getLambda();

        return L(cipherText.modPow(lambda, publicKey.getNSquare()), n)
            .multiply(mu)
            .mod(n);
    }

    public static BigInteger multiply(PaillierPublicKey publicKey, BigInteger cipherText, BigInteger plainText) {
        return cipherText.modPow(plainText, publicKey.getNSquare());
    }

    public static BigInteger add(PaillierPublicKey publicKey, BigInteger cipherText, BigInteger plainText) {
        return cipherText.multiply(publicKey.getG().modPow(plainText, publicKey.getNSquare()))
                         .mod(publicKey.getNSquare());
    }

    public static BigInteger addCipherTexts(PaillierPublicKey publicKey, BigInteger... cipherTexts) {
        return Stream.of(cipherTexts)
                     .reduce((c1, c2) -> addCipherTexts(publicKey, c1, c2))
                     .get();
    }

    public static BigInteger addCipherTexts(PaillierPublicKey publicKey, BigInteger cipherText1, BigInteger cipherText2) {
        return cipherText1.multiply(cipherText2)
                          .mod(publicKey.getNSquare());
    }

    // L(x)=(x-1)/n
    private static BigInteger L(BigInteger x, BigInteger n) {
        return x.subtract(ONE)
                .divide(n);
    }

}
