package ee.ut.msc.ecdsa2p.crypto.paillier;

import java.math.BigInteger;

import lombok.Data;

@Data
public class PaillierPrivateKey {

    private final BigInteger lambda;
    private final BigInteger mu;

}
