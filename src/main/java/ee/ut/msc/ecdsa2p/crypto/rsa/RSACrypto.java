package ee.ut.msc.ecdsa2p.crypto.rsa;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import lombok.SneakyThrows;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class RSACrypto {

    public static final int RECOMMENDED_KEY_SIZE = 3072;

    @SneakyThrows
    public static RSAKeyPair generateKeyPair(int keySize) {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", BouncyCastleProvider.PROVIDER_NAME);
        generator.initialize(keySize, new SecureRandom());
        KeyPair keyPair = generator.generateKeyPair();
        return new RSAKeyPair((RSAPrivateKey) keyPair.getPrivate(),
                              (RSAPublicKey) keyPair.getPublic());
    }

    public static RSAKeyPair generateKeyPair() {
        return generateKeyPair(RECOMMENDED_KEY_SIZE);
    }

    @SneakyThrows
    public static byte[] sign(RSAPrivateKey privateKey, byte[] message) {
        Signature signer = Signature.getInstance("SHA1withRSA", BouncyCastleProvider.PROVIDER_NAME);
        signer.initSign(privateKey);
        signer.update(message);
        return signer.sign();
    }

    @SneakyThrows
    public static boolean verify(RSAPublicKey publicKey, byte[] signature, byte[] message) {
        Signature signature1 = Signature.getInstance("SHA1withRSA", BouncyCastleProvider.PROVIDER_NAME);
        signature1.initVerify(publicKey);
        signature1.update(message);
        return signature1.verify(signature);
    }

}
