package ee.ut.msc.ecdsa2p.crypto.aes;

import static ee.ut.msc.ecdsa2p.util.MathUtils.sha3_256;

import java.nio.charset.StandardCharsets;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import lombok.SneakyThrows;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Hex;

public class AesCrypto {

    private static final String ALGORITHM = "AES";
    private static final String CIPHER    = "AES/ECB/NoPadding";

    @SneakyThrows
    public static byte[] encrypt(String key, byte[] toEncrypt) {
        Cipher cipher = Cipher.getInstance(CIPHER, BouncyCastleProvider.PROVIDER_NAME);
        cipher.init(Cipher.ENCRYPT_MODE, toSecretKey(key));
        return cipher.doFinal(toEncrypt);
    }

    @SneakyThrows
    public static byte[] decrypt(String key, byte[] encrypted) {
        Cipher cipher = Cipher.getInstance(CIPHER, BouncyCastleProvider.PROVIDER_NAME);
        cipher.init(Cipher.DECRYPT_MODE, toSecretKey(key));
        return cipher.doFinal(encrypted);
    }

    private static SecretKeySpec toSecretKey(String key) throws Exception {
        String sha3Hex = Hex.toHexString(sha3_256(key));
        String derivedKey = sha3Hex.substring(0, sha3Hex.length() / 4);
        return new SecretKeySpec(derivedKey.getBytes(StandardCharsets.UTF_8.name()), ALGORITHM);
    }

}
