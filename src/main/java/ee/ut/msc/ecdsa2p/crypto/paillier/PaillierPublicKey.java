package ee.ut.msc.ecdsa2p.crypto.paillier;

import java.math.BigInteger;

import lombok.Data;
import lombok.ToString.Exclude;

@Data
public class PaillierPublicKey {

    private final BigInteger n;
    private final BigInteger g;

    @Exclude
    private BigInteger nSquare;

    public BigInteger getNSquare() {
        if (nSquare == null) {
            nSquare = n.pow(2);
        }
        return nSquare;
    }
}
