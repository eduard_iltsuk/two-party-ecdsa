package ee.ut.msc.ecdsa2p.crypto.ec;

import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.params.ECDomainParameters;

public class EllipticCurve {

    public static final String             CURVE_TYPE = "secp256k1";
    public static final ECDomainParameters PARAMS;
    static {
        X9ECParameters params = SECNamedCurves.getByName(CURVE_TYPE);
        PARAMS = new ECDomainParameters(params.getCurve(), params.getG(), params.getN(), params.getH());
    }

}
