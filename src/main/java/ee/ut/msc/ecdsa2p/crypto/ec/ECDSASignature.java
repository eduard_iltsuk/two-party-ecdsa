package ee.ut.msc.ecdsa2p.crypto.ec;

import java.math.BigInteger;

import lombok.Data;

@Data
public class ECDSASignature {

    private final BigInteger r;
    private final BigInteger s;

}
