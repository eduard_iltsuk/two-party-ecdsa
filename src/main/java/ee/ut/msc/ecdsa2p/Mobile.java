package ee.ut.msc.ecdsa2p;

import static ee.ut.msc.ecdsa2p.util.MathUtils.randomInt;
import static ee.ut.msc.ecdsa2p.util.MathUtils.sha3_256;
import static java.math.BigInteger.ZERO;
import static org.bouncycastle.util.BigIntegers.fromUnsignedByteArray;

import java.math.BigInteger;

import ee.ut.msc.ecdsa2p.crypto.ec.ECCrypto;
import ee.ut.msc.ecdsa2p.crypto.ec.ECDSAPartialSignature;
import ee.ut.msc.ecdsa2p.crypto.ec.ECKeyPair;
import ee.ut.msc.ecdsa2p.crypto.ec.EllipticCurve;
import ee.ut.msc.ecdsa2p.crypto.paillier.PaillierCrypto;
import ee.ut.msc.ecdsa2p.crypto.paillier.PaillierPublicKey;
import org.bouncycastle.jce.interfaces.ECPrivateKey;
import org.bouncycastle.jce.interfaces.ECPublicKey;
import org.bouncycastle.math.ec.ECPoint;

public class Mobile {

    private ECPublicKey       commonPublicKey;
    private PaillierPublicKey paillierPublicKey;
    private BigInteger        mobilePrivateKey_encryptedByPin;
    private BigInteger        serverPrivateKey_encryptedByPaillier;

    // Temporarily stored parameters only for the duration of signing session
    private BigInteger kMobile;
    private ECPoint    rMobile;
    private BigInteger r;

    public ECKeyPair generateKeyPair(String pin) {
        ECKeyPair keyPair = ECCrypto.generateKeyPair();
        ECPrivateKey privateKey = keyPair.getPrivateKey();
        this.mobilePrivateKey_encryptedByPin = encryptPrivateKey(pin, privateKey.getD());
        return keyPair;
    }

    public ECPublicKey calculateCommonPublicKey(String pin, ECPublicKey serverPublicKey) {
        BigInteger privateKey = decryptPrivateKey(pin, mobilePrivateKey_encryptedByPin);
        this.commonPublicKey = ECCrypto.publicKeyFrom(serverPublicKey.getQ().multiply(privateKey));
        return commonPublicKey;
    }

    public void storePaillierPublicKey(PaillierPublicKey publicKey) {
        this.paillierPublicKey = publicKey;
    }

    public void storePaillierEncryptedServerPrivateKey(BigInteger encryptedServerPrivateKey) {
        this.serverPrivateKey_encryptedByPaillier = encryptedServerPrivateKey;
    }

    public ECDSAPartialSignature generatePartialSignature(ECPoint rServer, String pin, byte[] hash) {
        kMobile = randomInt(ZERO, EllipticCurve.PARAMS.getN());
        rMobile = EllipticCurve.PARAMS.getG().multiply(kMobile);
        ECPoint commonRPoint = rServer.multiply(kMobile).normalize();
        BigInteger rX = commonRPoint.getAffineXCoord().toBigInteger();
        r = rX.mod(EllipticCurve.PARAMS.getN());
        validateR(r);

        BigInteger z = fromUnsignedByteArray(hash);
        BigInteger p = randomInt(ZERO, EllipticCurve.PARAMS.getN().pow(2));
        BigInteger d_m = decryptPrivateKey(pin, mobilePrivateKey_encryptedByPin);
        BigInteger k_m_inverse = kMobile.modInverse(EllipticCurve.PARAMS.getN());

        BigInteger enc_s = computePartialSignature(d_m, z, p, k_m_inverse);

        return new ECDSAPartialSignature(rMobile, enc_s);
    }

    private BigInteger computePartialSignature(BigInteger dm, BigInteger z, BigInteger p, BigInteger k_m_inverse) {
        BigInteger s1_1 = z.multiply(k_m_inverse).mod(EllipticCurve.PARAMS.getN());
        BigInteger s1_2 = p.multiply(EllipticCurve.PARAMS.getN());
        BigInteger s1 = s1_1.add(s1_2);
        BigInteger enc_s1 = PaillierCrypto.encrypt(s1, paillierPublicKey);

        BigInteger s2_1 = r.multiply(dm).multiply(k_m_inverse).mod(EllipticCurve.PARAMS.getN());
        BigInteger enc_s2 = PaillierCrypto.multiply(paillierPublicKey, serverPrivateKey_encryptedByPaillier, s2_1);

        return PaillierCrypto.addCipherTexts(paillierPublicKey, enc_s1, enc_s2);
    }

    private BigInteger encryptPrivateKey(String pin, BigInteger d) {
        byte[] hash = sha3_256(pin);
        BigInteger derivedSecretKey = fromUnsignedByteArray(hash);
        return d.xor(derivedSecretKey);
    }

    private BigInteger decryptPrivateKey(String pin, BigInteger encryptedD) {
        byte[] hash = sha3_256(pin);
        BigInteger derivedSecretKey = fromUnsignedByteArray(hash);
        return encryptedD.xor(derivedSecretKey);
    }

    private void validateR(BigInteger r) {
        if (r.compareTo(ZERO) == 0) {
            throw new RuntimeException("R should not be zero");
        }
    }

}
