package ee.ut.msc.ecdsa2p.benchmark;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class Benchmark {

    public static final int ATTEMPTS = 10;

    public static void main(String[] args) {
       BenchmarkTwoPartyRsa rsa = new BenchmarkTwoPartyRsa();
       benchmark(() -> rsa.keyExchange());
       benchmark(() -> {rsa.keyExchange(); return rsa.sign();});
       benchmark(() -> {rsa.keyExchange(); rsa.sign();return rsa.verify();});

       BenchmarkTwoPartyEcdsa ecdsa = new BenchmarkTwoPartyEcdsa();
       benchmark(() -> ecdsa.keyExchange());
       benchmark(() -> {ecdsa.keyExchange(); return ecdsa.sign();});
       benchmark(() -> {ecdsa.keyExchange(); ecdsa.sign();return ecdsa.verify();});
    }

    public static void benchmark(Supplier<Long> supplier) {
        List<Long> times = new ArrayList<>();
        for (int i = 0; i < ATTEMPTS; i++) {
            times.add(supplier.get());
        }
        System.out.println("Min: " + min(times));
        System.out.println("Max: " + max(times));
        System.out.println("Average: " + average(times));
        System.out.println();
    }

    public static long average(List<Long> times) {
        return new Double(times.stream()
                               .mapToLong(t -> t)
                               .average()
                               .getAsDouble())
            .longValue();
    }

    public static long max(List<Long> times) {
        return times.stream()
                    .mapToLong(t -> t)
                    .max()
                    .getAsLong();
    }

    public static long min(List<Long> times) {
        return times.stream()
                    .mapToLong(t -> t)
                    .min()
                    .getAsLong();
    }

}
