package ee.ut.msc.ecdsa2p.benchmark;

import static ee.ut.msc.ecdsa2p.util.MathUtils.randomInt;
import static ee.ut.msc.ecdsa2p.util.MathUtils.sha3_256;
import static java.math.BigInteger.ZERO;
import static org.bouncycastle.util.BigIntegers.fromUnsignedByteArray;

import java.math.BigInteger;
import java.security.Security;

import ee.ut.msc.ecdsa2p.crypto.ec.ECCrypto;
import ee.ut.msc.ecdsa2p.crypto.ec.ECDSASignature;
import ee.ut.msc.ecdsa2p.crypto.ec.ECKeyPair;
import ee.ut.msc.ecdsa2p.crypto.ec.EllipticCurve;
import ee.ut.msc.ecdsa2p.crypto.paillier.PaillierCrypto;
import ee.ut.msc.ecdsa2p.crypto.paillier.PaillierKeyPair;
import ee.ut.msc.ecdsa2p.crypto.paillier.PaillierPrivateKey;
import ee.ut.msc.ecdsa2p.crypto.paillier.PaillierPublicKey;
import org.bouncycastle.jce.interfaces.ECPrivateKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.math.ec.ECPoint;

public class BenchmarkTwoPartyEcdsa {

    private ECPrivateKey       mobileEcPrivateKey;
    private ECPrivateKey       serverEcPrivateKey;
    private BigInteger         serverEcPrivateKey_encByPaillier;
    private ECPoint            compositePublicKey;
    private PaillierPublicKey  paillierPublicKey;
    private PaillierPrivateKey paillierPrivateKey;
    private BigInteger         r;
    private BigInteger         s;
    private byte[]             hash;

    public BenchmarkTwoPartyEcdsa() {
        Security.addProvider(new BouncyCastleProvider());
    }

    public long keyExchange() {
        long start = System.currentTimeMillis();
        ECKeyPair mobileEcKeyPair = ECCrypto.generateKeyPair();
        ECKeyPair serverEcKeyPair = ECCrypto.generateKeyPair();
        mobileEcPrivateKey = mobileEcKeyPair.getPrivateKey();
        serverEcPrivateKey = serverEcKeyPair.getPrivateKey();

        compositePublicKey = mobileEcKeyPair.getPublicKey().getQ().multiply(serverEcPrivateKey.getD());

        PaillierKeyPair paillierKeyPair = PaillierCrypto.generateKeyPair();
        paillierPublicKey = paillierKeyPair.getPublicKey();
        paillierPrivateKey = paillierKeyPair.getPrivateKey();

        serverEcPrivateKey_encByPaillier = PaillierCrypto.encrypt(serverEcPrivateKey.getD(), paillierPublicKey);

        long time = System.currentTimeMillis() - start;
        System.out.println("ECDSA key exchange took: " + time + " ms");
        return time;
    }

    public long sign() {
        long start = System.currentTimeMillis();
        BigInteger k_s = randomInt(ZERO, EllipticCurve.PARAMS.getN());
        BigInteger k_m = randomInt(ZERO, EllipticCurve.PARAMS.getN());
        ECPoint R_m = EllipticCurve.PARAMS.getG().multiply(k_m);

        this.r = R_m.multiply(k_s).normalize().getAffineXCoord().toBigInteger().mod(EllipticCurve.PARAMS.getN());

        this.hash = sha3_256("benchmark");
        BigInteger z = fromUnsignedByteArray(hash);
        BigInteger p = randomInt(ZERO, EllipticCurve.PARAMS.getN().pow(2));
        BigInteger k_m_inverse = k_m.modInverse(EllipticCurve.PARAMS.getN());

        BigInteger partial_s_encByPaillier = computePartialSignatureByYL(z, p, k_m_inverse);

        BigInteger partial_s = PaillierCrypto.decrypt(partial_s_encByPaillier, new PaillierKeyPair(paillierPublicKey, paillierPrivateKey));

        BigInteger s = partial_s.multiply(k_s.modInverse(EllipticCurve.PARAMS.getN()))
                                .mod(EllipticCurve.PARAMS.getN());

        this.s = s.min(EllipticCurve.PARAMS.getN().subtract(s));
        long time = System.currentTimeMillis() - start;
        System.out.println("ECDSA signing took: " + time + " ms");
        return time;
    }

    // Partial signature computation according to Yehuda Lindell
    private BigInteger computePartialSignatureByYL(BigInteger z, BigInteger p, BigInteger k_m_inverse) {
        BigInteger s1_1 = z.multiply(k_m_inverse).mod(EllipticCurve.PARAMS.getN());
        BigInteger s1_2 = p.multiply(EllipticCurve.PARAMS.getN());
        BigInteger s1 = s1_1.add(s1_2);
        BigInteger enc_s1 = PaillierCrypto.encrypt(s1, paillierPublicKey);

        BigInteger s2_1 = r.multiply(mobileEcPrivateKey.getD()).multiply(k_m_inverse).mod(EllipticCurve.PARAMS.getN());
        BigInteger enc_s2 = PaillierCrypto.multiply(paillierPublicKey, serverEcPrivateKey_encByPaillier, s2_1);
        return PaillierCrypto.addCipherTexts(paillierPublicKey, enc_s1, enc_s2);
    }

    // Sequential partial signature generation
    // enc(s') = p * n + (z + r * enc(server_pk) * mobile_pk) * mobile_k^(-1)
    private BigInteger computePartialSignature(BigInteger z, BigInteger p, BigInteger k_m_inverse) {
        BigInteger s1 = PaillierCrypto.multiply(paillierPublicKey, serverEcPrivateKey_encByPaillier, r);
        BigInteger s2 = PaillierCrypto.multiply(paillierPublicKey, s1, mobileEcPrivateKey.getD());
        BigInteger s3 = PaillierCrypto.add(paillierPublicKey, s2, z);
        BigInteger s4 = PaillierCrypto.multiply(paillierPublicKey, s3, k_m_inverse);
        return PaillierCrypto.add(paillierPublicKey, s4, p.multiply(EllipticCurve.PARAMS.getN()));
    }

    public long verify() {
        long start = System.currentTimeMillis();
        boolean verify = ECCrypto.verify(hash, new ECDSASignature(r, s), ECCrypto.publicKeyFrom(compositePublicKey));
        long time = System.currentTimeMillis() - start;
        System.out.println("ECDSA verification took: " + time + " ms");
        System.out.println(verify ? "VERIFIED" : "FAILED");
        System.out.println();
        return time;
    }


}
