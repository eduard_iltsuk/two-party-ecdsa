package ee.ut.msc.ecdsa2p.benchmark;

import static ee.ut.msc.ecdsa2p.util.MathUtils.randomInt;
import static ee.ut.msc.ecdsa2p.util.MathUtils.sha3_256;
import static java.math.BigInteger.ZERO;
import static org.bouncycastle.util.BigIntegers.fromUnsignedByteArray;

import java.math.BigInteger;
import java.security.Security;
import java.security.interfaces.RSAPrivateCrtKey;

import ee.ut.msc.ecdsa2p.crypto.rsa.RSACrypto;
import ee.ut.msc.ecdsa2p.crypto.rsa.RSAKeyPair;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.pqc.math.ntru.euclid.BigIntEuclidean;

public class BenchmarkTwoPartyRsa {

    private BigInteger d_s;
    private BigInteger d_m2;
    private BigInteger d_m1;
    private BigInteger n;
    private BigInteger n_m;
    private BigInteger n_s;
    private BigInteger alpha;
    private BigInteger beta;
    private BigInteger e;
    private BigInteger s;
    private BigInteger m;

    public BenchmarkTwoPartyRsa() {
        Security.addProvider(new BouncyCastleProvider());
    }

    public long keyExchange() {
        long start = System.currentTimeMillis();
        RSAKeyPair mobileKeyPair = RSACrypto.generateKeyPair();

        RSAPrivateCrtKey crt = (RSAPrivateCrtKey) mobileKeyPair.getPrivateKey();
        BigInteger ph_n_m = crt.getPrimeP()
                               .subtract(BigInteger.ONE)
                               .multiply(crt.getPrimeQ().subtract(BigInteger.ONE));

        BigInteger d_m = mobileKeyPair.getPrivateKey().getPrivateExponent();
        n_m = mobileKeyPair.getPublicKey().getModulus();
        e = mobileKeyPair.getPublicKey().getPublicExponent();

        BigInteger max = new BigInteger("2").pow(n_m.bitLength());
        d_m1 = randomInt(ZERO, max);
        d_m2 = d_m.subtract(d_m1).mod(ph_n_m);

        RSAKeyPair serverKeyPair = RSACrypto.generateKeyPair();
        n_s = serverKeyPair.getPublicKey().getModulus();
        d_s = serverKeyPair.getPrivateKey().getPrivateExponent();

        BigIntEuclidean euclidean = BigIntEuclidean.calculate(n_m, n_s);
        alpha = euclidean.x;
        beta = euclidean.y;
        n = n_s.multiply(n_m);

        long time = System.currentTimeMillis() - start;
        System.out.println("RSA key exchange took: " + time + " ms");
        return time;
    }

    public long sign() {
        long start = System.currentTimeMillis();
        m = fromUnsignedByteArray(sha3_256("benchmark"));

        BigInteger s_m1 = m.modPow(d_m1, n_m);

        BigInteger s_m2 = m.modPow(d_m2, n_m);
        BigInteger s_m = s_m1.multiply(s_m2).mod(n_m);
        BigInteger s_s = m.modPow(d_s, n_s);
        BigInteger s_part1 = beta.multiply(n_s).multiply(s_m);
        BigInteger s_part2 = alpha.multiply(n_m).multiply(s_s);

        s = s_part1.add(s_part2).mod(n);
        long time = System.currentTimeMillis() - start;
        System.out.println("RSA signing took: " + time + " ms");
        return time;
    }

    public long verify() {
        long start = System.currentTimeMillis();
        BigInteger verify = s.modPow(e, n);
        long time = System.currentTimeMillis() - start;
        System.out.println("RSA verification took: " + time + " ms");
        System.out.println(verify.equals(m) ? "VERIFIED" : "FAILED");
        System.out.println();
        return time;
    }

}
