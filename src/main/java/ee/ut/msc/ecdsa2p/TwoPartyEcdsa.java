package ee.ut.msc.ecdsa2p;

import static ee.ut.msc.ecdsa2p.util.MathUtils.sha3_256;

import java.math.BigInteger;
import java.security.Security;

import ee.ut.msc.ecdsa2p.crypto.ec.ECCrypto;
import ee.ut.msc.ecdsa2p.crypto.ec.ECDSAPartialSignature;
import ee.ut.msc.ecdsa2p.crypto.ec.ECDSASignature;
import ee.ut.msc.ecdsa2p.crypto.ec.ECKeyPair;
import ee.ut.msc.ecdsa2p.crypto.paillier.PaillierKeyPair;
import ee.ut.msc.ecdsa2p.crypto.paillier.PaillierPublicKey;
import org.bouncycastle.jce.interfaces.ECPublicKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.math.ec.ECPoint;
import org.junit.jupiter.api.Assertions;

public class TwoPartyEcdsa {

    private static final String CORRECT_PIN = "1234";

    public static void main(String[] args) {
        Security.addProvider(new BouncyCastleProvider());
        byte[] hash = sha3_256("hello world");

        Mobile mobile = new Mobile();
        Server server = new Server();

        PaillierKeyPair serverPaillierKeyPair = server.generatePaillierKeyPair();
        System.out.println("Server generated Paillier key pair:");
        System.out.println("private key -> " + serverPaillierKeyPair.getPrivateKey());
        System.out.println("public key -> " + serverPaillierKeyPair.getPublicKey());
        System.out.println();

        ECKeyPair mobileEcKeyPair = mobile.generateKeyPair(CORRECT_PIN);
        System.out.println("Mobile generated EC key pair:");
        System.out.println("private key -> " + mobileEcKeyPair.getPrivateKey().getD());
        System.out.println("public key -> " + mobileEcKeyPair.getPublicKey().getQ());
        System.out.println();

        ECKeyPair serverEcKeyPair = server.generateECKeyPair();
        System.out.println("Server generated EC key pair:");
        System.out.println("private key -> " + serverEcKeyPair.getPrivateKey().getD());
        System.out.println("public key -> " + mobileEcKeyPair.getPublicKey().getQ());
        System.out.println();

        System.out.println("Starting EC phase...");
        ECPublicKey commonPublicKey = server.calculateCommonPublicKey(mobileEcKeyPair.getPublicKey());
        System.out.println("Common public key: " + commonPublicKey.getQ());
        System.out.println();

        System.out.println("Starting Paillier phase...");
        mobile.storePaillierPublicKey(serverPaillierKeyPair.getPublicKey());
        BigInteger encryptedServerPrivateKey = server.getPaillierEncryptedServerPrivateKey();
        System.out.println("Paillier ciphertext of Server EC private key: " + encryptedServerPrivateKey);
        mobile.storePaillierEncryptedServerPrivateKey(encryptedServerPrivateKey);
        System.out.println("Key exchange finished...");
        System.out.println();

        System.out.println("Starting signing...");
        ECPoint rServer = server.generateServerKAndR();
        ECDSAPartialSignature partialSignature = mobile.generatePartialSignature(rServer, CORRECT_PIN, hash);
        System.out.println("Mobile signed.");
        System.out.println("Encrypted partial signature: " + partialSignature.getEncryptedPartialS());

        ECDSASignature signature = server.generateSignature(partialSignature);
        System.out.println("Server signed.");
        System.out.println("Final signature: " + signature);

        boolean verified = ECCrypto.verify(hash, signature, commonPublicKey);
        if (verified) {
            System.out.println("Verification successful!");
        }
        Assertions.assertTrue(verified);
        System.out.println("Signing finished.");
    }

}
