package ee.ut.msc.ecdsa2p.util;

import static java.math.BigInteger.ONE;
import static java.math.BigInteger.ZERO;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.SecureRandom;

import lombok.SneakyThrows;
import org.bouncycastle.util.BigIntegers;

public class MathUtils {

    public static final SecureRandom RANDOM = new SecureRandom();

    public static BigInteger randomPrimeInt(int bitLength) {
        return new BigInteger(bitLength, 100, RANDOM);
    }

    public static BigInteger randomInt(BigInteger minExclusive, BigInteger maxExclusive) {
        return BigIntegers.createRandomInRange(minExclusive.add(ONE),
                                               maxExclusive.subtract(ONE),
                                               RANDOM);
    }

    public static BigInteger randomCoprimeIntLessThan(BigInteger maxValue) {
        BigInteger result;
        do {
            result = randomInt(ZERO, maxValue);
        } while (!coprime(result, maxValue));
        return result;
    }

    public static boolean coprime(BigInteger value1, BigInteger value2) {
        return value1.gcd(value2).equals(ONE);
    }

    public static byte[] sha3_256(String input) {
        return sha3_256(input.getBytes(StandardCharsets.UTF_8));
    }

    public static byte[] sha3_256(byte[] input) {
        return digest(input, "SHA3-256");
    }

    @SneakyThrows
    private static byte[] digest(byte[] input, String algorithm) {
        return MessageDigest.getInstance(algorithm, "BC")
                            .digest(input);
    }

}
